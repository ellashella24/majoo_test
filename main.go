package main

import (
	"majoo_test/database"
	"majoo_test/routes"
)

func main() {
	database.Init()

	e := routes.Init()

	e.Logger.Fatal(e.Start(":5000"))
}
