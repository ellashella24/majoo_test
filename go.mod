module majoo_test

go 1.15

require (
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/vcs v1.13.1 // indirect
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/canthefason/go-watcher v0.2.4 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.10.0 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-playground/validator/v10 v10.5.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/dep v0.5.4 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jmank88/nuts v0.4.0 // indirect
	github.com/labstack/echo/v4 v4.2.2
	github.com/nightlyone/lockfile v1.0.0 // indirect
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sdboyer/constext v0.0.0-20170321163424-836a14457353 // indirect
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210423185535-09eb48e85fd7 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
