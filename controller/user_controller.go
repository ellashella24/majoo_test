package controller

import (
	"fmt"
	"io"
	"majoo_test/models"
	"net/http"
	"os"
	"strconv"

	"github.com/labstack/echo/v4"
)

func GetAll(c echo.Context) error {
	result, err := models.GetAll()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}

func CreateUser(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	namaLengkap := c.FormValue("namaLengkap")
	foto := c.FormValue("foto")

	result, err := models.CreateUser(username, password, namaLengkap, foto)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}

func UpdateUser(c echo.Context) error {
	id := c.FormValue("id")
	username := c.FormValue("username")
	password := c.FormValue("password")
	namaLengkap := c.FormValue("namaLengkap")
	foto := c.FormValue("foto")

	conv_id, err := strconv.Atoi(id)

	result, err := models.UpdateUser(conv_id, username, password, namaLengkap, foto)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}

func UpdatePicture(c echo.Context) error {
	id := c.FormValue("id")
	foto, err := c.FormFile("foto")

	if err != nil {
		return err
	}

	src, err := foto.Open()

	if err != nil {
		return err
	}

	defer src.Close()

	mimeType := foto.Header.Get("Content-Type")

	allowedMime := map[string]bool{
		"image/jpg":  true,
		"image/jpeg": true,
		"image/png":  true,
	}

	if !allowedMime[mimeType] {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "File type isn't allowed")
	}

	dst, err := os.Create("upload/" + foto.Filename)
	if err != nil {
		fmt.Println(err)
		return err
	}
	
	defer dst.Close()

	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	conv_id, err := strconv.Atoi(id)
	fotoProfile := foto.Filename

	result, err := models.UpdatePicture(conv_id, fotoProfile)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}

func DeleteUser(c echo.Context) error {
	id := c.FormValue("id")

	conv_id, err := strconv.Atoi(id)

	result, err := models.DeleteUser(conv_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}
