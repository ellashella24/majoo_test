package routes

import (
	"majoo_test/controller"
	"majoo_test/middleware"
	"net/http"

	"github.com/labstack/echo/v4"
)

func Init() *echo.Echo {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Majoo Test")
	})

	e.GET("/user/all", controller.GetAll, middleware.IsAuthenticated)
	e.POST("/user/create", controller.CreateUser, middleware.IsAuthenticated)
	e.PUT("/user/update", controller.UpdateUser, middleware.IsAuthenticated)
	e.DELETE("/user/delete", controller.DeleteUser, middleware.IsAuthenticated)

	e.POST("/user/login", controller.CheckLogin)
	e.POST("/user/updatePicture", controller.UpdatePicture, middleware.IsAuthenticated)

	return e
}
