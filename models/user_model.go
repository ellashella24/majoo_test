package models

import (
	"majoo_test/database"
	"majoo_test/helpers"
	"net/http"

	validator "github.com/go-playground/validator/v10"
)

type User struct {
	Id          int    `json:"id"`
	Username    string `json:"username" validate:"required"`
	Password    string `json:"password" validate:"required"`
	NamaLengkap string `json:"namaLengkap" validate:"required"`
	Foto        string `json:"foto"`
}

func GetAll() (Response, error) {
	var obj User
	var arrobj []User
	var res Response

	con := database.CreateCon()

	sqlStatement := "SELECT * FROM user"

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {
		err = rows.Scan(&obj.Id, &obj.Username, &obj.Password, &obj.NamaLengkap, &obj.Foto)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrobj

	return res, nil

}

func CreateUser(username string, password string, namaLengkap string, foto string) (Response, error) {
	var res Response

	v := validator.New()

	obj := User{
		Username:    username,
		Password:    password,
		NamaLengkap: namaLengkap,
		Foto:        foto,
	}

	err := v.Struct(obj)

	if err != nil {
		return res, err
	}

	con := database.CreateCon()

	convPassword, _ := helpers.HashPassword(password)

	sqlStatement := "insert into user (username, password, nama_lengkap, foto) value (?, ?, ?, ?)"

	stmt, err := con.Prepare(sqlStatement)

	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(username, convPassword, namaLengkap, foto)

	if err != nil {
		return res, err
	}

	lastInsertedId, err := result.LastInsertId()

	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"last_inserted_id": lastInsertedId,
	}

	return res, err

}

func UpdateUser(id int, username string, password string, namaLengkap string, foto string) (Response, error) {
	var res Response

	con := database.CreateCon()

	convPassword, _ := helpers.HashPassword(password)

	sqlStatement := "UPDATE user SET username = ?, password = ?, nama_lengkap = ?, foto = ? WHERE id = ? "

	stmt, err := con.Prepare(sqlStatement)

	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(username, convPassword, namaLengkap, foto, id)

	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()

	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"Rows Affected": rowsAffected,
	}

	return res, nil

}

func UpdatePicture(id int, foto string) (Response, error) {
	var res Response

	con := database.CreateCon()

	sqlStatement := "UPDATE user SET foto = ? WHERE id = ? "

	stmt, err := con.Prepare(sqlStatement)

	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(foto, id)

	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()

	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"Rows Affected": rowsAffected,
	}

	return res, nil

}

func DeleteUser(id int) (Response, error) {
	var res Response

	con := database.CreateCon()

	sqlStatement := "DELETE FROM user WHERE id = ? "

	stmt, err := con.Prepare(sqlStatement)

	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(id)

	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()

	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"Rows Affected": rowsAffected,
	}

	return res, nil

}
