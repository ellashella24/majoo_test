package models

import (
	"database/sql"
	"fmt"
	"majoo_test/database"
	"majoo_test/helpers"
)

type UserLogin struct {
	Id          int    `json: "id"`
	Username    string `json: "username"`
	Password    string `json: "password"`
	NamaLengkap string `json: "namalengkap"`
	Foto        string `json: "foto"`
}

func CheckLogin(username, password string) (bool, error) {
	var obj UserLogin
	var pwd string

	con := database.CreateCon()

	sqlStatement := "SELECT * FROM user WHERE username = ?"

	err := con.QueryRow(sqlStatement, username).Scan(
		&obj.Id, &obj.Username, &pwd, &obj.NamaLengkap, &obj.Foto,
	)

	if err == sql.ErrNoRows {
		fmt.Println("Username not found")
		return false, err
	}

	if err != nil {
		fmt.Println("Query Error")
		return false, err
	}

	match, err := helpers.CheckPasswordHash(password, pwd)
	if !match {
		fmt.Println("Password doesn't match")
		return false, err
	}

	return true, nil

}
